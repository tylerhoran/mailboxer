class AddDiscussionOptout < ActiveRecord::Migration
  def self.up
    create_table :mailboxer_discussion_opt_outs do |t|
      t.references :unsubscriber, :polymorphic => true
      t.references :discussion
    end
    add_foreign_key "mailboxer_discussion_opt_outs", "mailboxer_discussions", :name => "mb_opt_outs_on_discussions_id", :column => "discussion_id"
  end

  def self.down
    remove_foreign_key "mailboxer_discussion_opt_outs", :name => "mb_opt_outs_on_discussions_id"
    drop_table :mailboxer_discussion_opt_outs
  end
end
