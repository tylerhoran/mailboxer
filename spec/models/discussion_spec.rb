require 'spec_helper'

describe Mailboxer::discussion do

  let!(:entity1)  { FactoryGirl.create(:user) }
  let!(:entity2)  { FactoryGirl.create(:user) }
  let!(:receipt1) { entity1.send_message(entity2,"Body","Subject") }
  let!(:receipt2) { entity2.reply_to_all(receipt1,"Reply body 1") }
  let!(:receipt3) { entity1.reply_to_all(receipt2,"Reply body 2") }
  let!(:receipt4) { entity2.reply_to_all(receipt3,"Reply body 3") }
  let!(:message1) { receipt1.notification }
  let!(:message4) { receipt4.notification }
  let!(:discussion) { message1.discussion }

  it { should validate_presence_of :subject }
  it { should ensure_length_of(:subject).is_at_most(Mailboxer.subject_max_length) }

  it "should have proper original message" do
    discussion.original_message.should == message1
  end

  it "should have proper originator (first sender)" do
    discussion.originator.should == entity1
  end

  it "should have proper last message" do
    discussion.last_message.should == message4
  end

  it "should have proper last sender" do
    discussion.last_sender.should == entity2
  end

  it "should have all discussion users" do
    discussion.recipients.count.should == 2
    discussion.recipients.count.should == 2
    discussion.recipients.count(entity1).should == 1
    discussion.recipients.count(entity2).should == 1
  end

  it "should be able to be marked as deleted" do
    discussion.move_to_trash(entity1)
    discussion.mark_as_deleted(entity1)
    discussion.should be_is_deleted(entity1)
  end

  it "should be removed from the database once deleted by all participants" do
    discussion.mark_as_deleted(entity1)
    discussion.mark_as_deleted(entity2)
    Mailboxer::discussion.exists?(discussion.id).should be_false
  end

  it "should be able to be marked as read" do
    discussion.mark_as_read(entity1)
    discussion.should be_is_read(entity1)
  end

  it "should be able to be marked as unread" do
    discussion.mark_as_read(entity1)
    discussion.mark_as_unread(entity1)
    discussion.should be_is_unread(entity1)
  end

  it "should be able to add a new participant" do
    new_user = FactoryGirl.create(:user)
    discussion.add_participant(new_user)
    discussion.participants.count.should == 3
    discussion.participants.should include(new_user, entity1, entity2)
    discussion.receipts_for(new_user).count.should == discussion.receipts_for(entity1).count
  end

  it "should deliver messages to new participants" do
    new_user = FactoryGirl.create(:user)
    discussion.add_participant(new_user)
    expect{
      receipt5 = entity1.reply_to_all(receipt4,"Reply body 4")
    }.to change{ discussion.receipts_for(new_user).count }.by 1
  end

  describe "scopes" do
    let(:participant) { FactoryGirl.create(:user) }
    let!(:inbox_discussion) { entity1.send_message(participant, "Body", "Subject").notification.discussion }
    let!(:sentbox_discussion) { participant.send_message(entity1, "Body", "Subject").notification.discussion }


    describe ".participant" do
      it "finds discussions with receipts for participant" do
        Mailboxer::discussion.participant(participant).should == [sentbox_discussion, inbox_discussion]
      end
    end

    describe ".inbox" do
      it "finds inbox discussions with receipts for participant" do
        Mailboxer::discussion.inbox(participant).should == [inbox_discussion]
      end
    end

    describe ".sentbox" do
      it "finds sentbox discussions with receipts for participant" do
        Mailboxer::discussion.sentbox(participant).should == [sentbox_discussion]
      end
    end

    describe ".trash" do
      it "finds trash discussions with receipts for participant" do
        trashed_discussion = entity1.send_message(participant, "Body", "Subject").notification.discussion
        trashed_discussion.move_to_trash(participant)

        Mailboxer::discussion.trash(participant).should == [trashed_discussion]
      end
    end

    describe ".unread" do
      it "finds unread discussions with receipts for participant" do
        [sentbox_discussion, inbox_discussion].each {|c| c.mark_as_read(participant) }
        unread_discussion = entity1.send_message(participant, "Body", "Subject").notification.discussion

        Mailboxer::discussion.unread(participant).should == [unread_discussion]
      end
    end
  end

  describe "#is_completely_trashed?" do
    it "returns true if all receipts in discussion are trashed for participant" do
      discussion.move_to_trash(entity1)
      discussion.is_completely_trashed?(entity1).should be_true
    end
  end

  describe "#is_deleted?" do
    it "returns false if a recipient has not deleted the discussion" do
      discussion.is_deleted?(entity1).should be_false
    end

    it "returns true if a recipient has deleted the discussion" do
      discussion.mark_as_deleted(entity1)
      discussion.is_deleted?(entity1).should be_true
    end
  end

  describe "#is_orphaned?" do
    it "returns true if both participants have deleted the discussion" do
      discussion.mark_as_deleted(entity1)
      discussion.mark_as_deleted(entity2)
      discussion.is_orphaned?.should be_true
    end

    it "returns false if one has not deleted the discussion" do
      discussion.mark_as_deleted(entity1)
      discussion.is_orphaned?.should be_false
    end
  end


  describe "#opt_out" do
    context 'participant still opt in' do
      let(:opt_out) { discussion.opt_outs.first }

      it "creates an opt_out object" do
        expect{
          discussion.opt_out(entity1)
        }.to change{ discussion.opt_outs.count}.by 1
      end

      it "creates opt out object linked to the proper discussion and participant" do
        discussion.opt_out(entity1)
        expect(opt_out.discussion).to eq discussion
        expect(opt_out.unsubscriber).to eq entity1
      end
    end

    context 'participant already opted out' do
      before do
        discussion.opt_out(entity1)
      end
      it 'does nothing' do
        expect{
          discussion.opt_out(entity1)
        }.to_not change{ discussion.opt_outs.count}
      end
    end
  end

  describe "#opt_out" do
    context 'participant already opt in' do
      it "does nothing" do
        expect{
          discussion.opt_in(entity1)
        }.to_not change{ discussion.opt_outs.count }
      end
    end

    context 'participant opted out' do
      before do
        discussion.opt_out(entity1)
      end
      it 'destroys the opt out object' do
        expect{
          discussion.opt_in(entity1)
        }.to change{ discussion.opt_outs.count}.by -1
      end
    end
  end

  describe "#subscriber?" do
    let(:action) { discussion.has_subscriber?(entity1) }

    context 'participant opted in' do
      it "returns true" do
        expect(action).to be_true
      end
    end

    context 'participant opted out' do
      before do
        discussion.opt_out(entity1)
      end
      it 'returns false' do
        expect(action).to be_false
      end
    end
  end

end
