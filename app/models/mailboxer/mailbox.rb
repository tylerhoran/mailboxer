class Mailboxer::Mailbox
  attr_accessor :type
  attr_reader :messageable

  #Initializer method
  def initialize(messageable)
    @messageable = messageable
  end

  #Returns the notifications for the messageable
  def notifications(options = {})
    #:type => nil is a hack not to give Messages as Notifications
    notifs = Mailboxer::Notification.recipient(@messageable).where(:type => nil).order("mailboxer_notifications.created_at DESC")
    if (options[:read].present? and options[:read]==false) or (options[:unread].present? and options[:unread]==true)
      notifs = notifs.unread
    end

    notifs
  end

  #Returns the discussions for the messageable
  #
  #Options
  #
  #* :mailbox_type
  #  * "inbox"
  #  * "sentbox"
  #  * "trash"
  #
  #* :read=false
  #* :unread=true
  #
  def discussions(options = {})
    conv = Mailboxer::Discussion.participant(@messageable)

    if options[:mailbox_type].present?
      case options[:mailbox_type]
      when 'inbox'
        conv = Mailboxer::Discussion.inbox(@messageable)
      when 'sentbox'
        conv = Mailboxer::Discussion.sentbox(@messageable)
      when 'trash'
        conv = Mailboxer::Discussion.trash(@messageable)
      when  'not_trash'
        conv = Mailboxer::Discussion.not_trash(@messageable)
      end
    end

    if (options.has_key?(:read) && options[:read]==false) || (options.has_key?(:unread) && options[:unread]==true)
      conv = conv.unread(@messageable)
    end

    conv
  end

  #Returns the Discussions in the inbox of messageable
  #
  #Same as Discussions({:mailbox_type => 'inbox'})
  def inbox(options={})
    options = options.merge(:mailbox_type => 'inbox')
    self.discussions(options)
  end

  #Returns the Discussions in the sentbox of messageable
  #
  #Same as discussions({:mailbox_type => 'sentbox'})
  def sentbox(options={})
    options = options.merge(:mailbox_type => 'sentbox')
    self.discussions(options)
  end

  #Returns the Discussions in the trash of messageable
  #
  #Same as discussions({:mailbox_type => 'trash'})
  def trash(options={})
    options = options.merge(:mailbox_type => 'trash')
    self.discussions(options)
  end

  #Returns all the receipts of messageable, from Messages and Notifications
  def receipts(options = {})
    Mailboxer::Receipt.where(options).recipient(@messageable)
  end

  #Deletes all the messages in the trash of messageable. NOT IMPLEMENTED.
  def empty_trash(options = {})
    #TODO
    false
  end

  #Returns if messageable is a participant of discussions
  def has_discussion?(discussion)
    discussion.is_participant?(@messageable)
  end

  #Returns true if messageable has at least one trashed message of the discussion
  def is_trashed?(discussion)
    discussion.is_trashed?(@messageable)
  end

  #Returns true if messageable has trashed all the messages of the discussion
  def is_completely_trashed?(discussion)
    discussion.is_completely_trashed?(@messageable)
  end

  #Returns the receipts of object for messageable as a ActiveRecord::Relation
  #
  #Object can be:
  #* A Message
  #* A Notification
  #* A Discussion
  #
  #If object isn't one of the above, a nil will be returned
  def receipts_for(object)
    case object
    when Mailboxer::Message, Mailboxer::Notification
      object.receipt_for(@messageable)
    when Mailboxer::Discussion
      object.receipts_for(@messageable)
    end
  end
end
